const express = require('express');
const app = express();
const PORT = 8000;
app.use(
    express.urlencoded({extended:true})
);

app.get('/usuario', (req, res) => {
    let id = req.query.id;
    let nome = req.query.nome;
    let sobrenome = req.query.sobrenome;

    if(id<0){
        res.status(404).send("Id Inválido")
    }else{
        res.status(200).send("MÉTODO GET (QUERY) - /USUARIO " + nome);
    }
    console.log("MÉTODO GET - /USUARIO");
});

app.get('/usuario/:id/:nome/:sobrenome', (req, res) =>{
    console.log(req);
    let id = req.params.id;
    let nome = req.params.nome;
    let sobrenome = req.params.sobrenome;
    res.status(200).send("MÉTODO GET (PARAM)- /USUARIO " + nome);
});

app.get('/usuario/form', (req , res)=>{
    console.log(req.body);
    res.status(200).send("MÉTODO GET (BODY)- /USUARIO ");
});

app.post('/usuario', (req, res) => {
    console.log(req);
    console.log(JSON.stringify(req.query));
    let id = req.query.id;
    let nome = req.query.nome;
    let sobrenome = req.query.sobrenome;
    res.send("MÉTODO POST - /USUARIO - id: " + id + " - nome: " + nome + "- sobrenome: " + sobrenome);
    console.log("MÉTODO POST - /USUARIO");
});

app.put('/usuario', (req, res) => {
    res.send("MÉTODO PUT - /USUARIO");
    console.log("MÉTODO PUT - /USUARIO");
});

app.delete('/usuario', (req, res) => {
    res.send("MÉTODO DELETE - /USUARIO");
    console.log("MÉTODO DELETE - /USUARIO");
});

//CRUD = CREATE READ UPDATE DELETE

app.get('/cliente', (req, res) => {
    res.send("MÉTODO GET - cliente");
    console.log("MÉTODO GET - cliente");
});

app.post('/cliente', (req, res) => {
    res.send("MÉTODO GET - cliente");
    console.log("MÉTODO GET - cliente");
});

app.listen(PORT,()=>{
    console.log(`PROJETO INICIADO NA PORTA ${PORT}`);
});